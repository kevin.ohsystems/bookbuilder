---
License: GNU All-Permissive License
License Notice: Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.
---

# A git-based book (or learning path) builder

## Background

My work with learning systems and platforms recently congregated with the need for a handbook or manual for an organization (how this organization works). Both topics are within the realm of _knowledge management_ with the help of IT.

The problem with most _learning management systems_ for example is, that the content is not separate from the course or learning path. Thus it cannot be freely mixed and matched. Also there is usually no API to the content, so we cannot re-use it in any application we may want to use or build.

This project explores new ways to address this, with as little development work as needed. Meaning if there is existing infrastructure we can use while respecting our principals we should probably not build new tools.

## Why

We need a system to collaboratively build books or learning systems that allow us to utilize the advantages of the Internet while retaining **freedom** and **ownership**[^1] over our content.

We also want to be able to **re-use** each chapter or section of our work to **compile different publications** (website, pdf. folder...)

Git is perfect in many ways, as (online-)collaboration, version control and offline work are all covered.

Gitlab and Github provide out-of-the-box **markdown rendering** and APIs to access the individual chapters.

## Principals

1. Independence from commercial vendors / platforms / tools (commercial software) (Open Source / Free Software) where reasonable (at least make sure a switch to another vendor is easy)
2. Easy to use and easy to learn text editing language, markup with minimal complexity
3. Re-Use content (chapters, sections) — where possible without duplication
4. Change once, update everywhere. Changes to content files need to take effect automatically in the books (some kind of dynamic build from api on view request...) 
5. Open formats and standards
6. Allow linking and indexing of sorts for semantic connections of content (chapters and sections)
7. Environmental friendly data and technology usage (link and re-use instead of duplicate storage)
8. Respect authors that work on different operating systems (Windows, Mac, Linux). Using the Browser for everything is also not a given in the context of this project

## User Flow Example

![User Flow](./img/userflow-a-b.png)

#### Footnotes

[^1]: Freedom and ownership meaning we can download, store and edit our own work with at least one (and ideally many) freely available standard   Software, no matter which of the common computer operating system we use. 
