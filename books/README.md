---
License: GNU All-Permissive License
License Notice: Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
---

# A git-based book (or learning path) builder

This directory is part of the [bookbuilder project](https://gitlab.com/kevin.ohsystems/bookbuilder)

## Directory for Book Assembly Files

This directory is for files that construct a book or a learning path (different name, same concept).
Each file contains links to the individual content sections or chapters (different name, same concept).

### Book Assembly File Structure

This is work in progress. The initial idea is to explore links to the file in gitlab through the API, for example [https://gitlab.com/api/v4/projects/24935949/repository/files/README.md/raw?ref=main](https://gitlab.com/api/v4/projects/24935949/repository/files/README.md/raw?ref=main)
So we can construct webpages with it in future, to assemble the actual pages from content and display them, rather than just showing links.

Just adding internal links would mean possible duplication of files, as every section of the book would need to be stored inside the same project. 

We want to be able to assemble books from all kinds of gitlab projects / sources, so the API seems the way to go.
