---
License: GNU All-Permissive License
License Notice: Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
---

# A git-based book (or learning path) builder

This directory is part of the [bookbuilder project](https://gitlab.com/kevin.ohsystems/bookbuilder)

## Directory for Book content Files aka Sections (or Chapters)

This directory contains individual thoughts, instructions etc. Meaning **sections** that could be combined to a "publication" or "book" or whatever you like to call the combination of several sections.

### Use Markdown

For our explorations and testing we work with basic [Markdown](https://en.wikipedia.org/wiki/Markdown), although _footnotes_ are already beyond "basic Markdown" afaik, and I already use those in this project.
