# 2. creating a public project

Date: 2021-03-07

## Status

Accepted

## Context

In order to test my ideas with gitlab while reflecting on my ideas and making public / open for collaboration I created this gitlab project

## Decision

Create a public project on gitlab

## Consequences

Opening up for collaboration, for anyone to contribute, critizise and possibly use this idea
