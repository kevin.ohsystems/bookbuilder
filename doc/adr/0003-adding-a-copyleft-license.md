# 3. adding a copyleft license

Date: 2021-03-07

## Status

Accepted

## Context

I want to make clear my intention to share my idea, to open it up for contributions

## Decision

Adding the GNU All-Permissive License to the README.md file

## Consequences

Awareness for the license is possibly created
