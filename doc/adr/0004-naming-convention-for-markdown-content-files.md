# 4. naming convention for markdown content files

Date: 2021-03-07

## Status

Accepted

## Context

Need to have a common sensible naming convention to ease book assembly

## Decision

Going with a UTC datestamp (date -u +'%d%m%Y%H%M%S'plus descriptive name, for example 070320211209-markdown-cheatsheet.md

## Consequences

Will need a simple way for users to create timestamp, maybe difficult for non tech-savvy users
