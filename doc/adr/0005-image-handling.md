# 5. image handling

Date: 2021-03-16

## Status

Accepted

## Context

I thought images in git not a good idea, so wanted to solve the problem of images first up

## Decision

After a chat with Chregu Stocker of Liip I decided to "abstract away" this problem for now and deal with it later.

## Consequences

Images for the prototype will reside in img dirs inside the projects content dir
